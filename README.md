# COMBINE

## Description
The WENR Combine tool combines two or more categorical geospatial rasters. It creates a new geospatial integer raster in which each value points to a unique combination of categories of the input rasters. The key to the unique combinations is provided in a *.csv table. 

This is an open source adaptation of the ArcGIS Pro [Combine Tool](https://pro.arcgis.com/en/pro-app/latest/tool-reference/spatial-analyst/combine.htm).

The illustration below demonstrates the Combine tool. 

![combined_raster](resources/demo_combined_raster.JPG)

Left and center are two integer arrays, called 'beheertypen' and 'top10'. These are the two input rasters. The Combine tool output is shown on the right. It is an array of euqal dimensions. The values in it point to specific combinations of 'beheertypen' and 'top10'. The associated table would look like:

|value|beheertypen|top10|
|-----|-----|-----|
|1|11|142|
|2|13|160|
|3|11|143|
|4|13|144|
|5|10|100|
|6|10|143|
|7|10|160|
|8|11|144|
|9|12|161|
|10|11|100|

The combine tool is designed for use in conjunction with the [Multi-Reclass Tool](https://git.wur.nl/roelo008/mrt).

## Installation
The Combine tool requires a Python environment with the following modules installed:

* numpy
* pandas
* rasterio
* prefect 1.0

## Usage
Use the Command Line Interface to handle the Combine tool. Positional arguments are:

```
positional arguments:
  destination    path to destination *.tif and *.csv files
  sources        path to source file(s)

optional arguments:
  -h, --help       show this help message and exit
  --test           test run with 20 windows
  --mrt_template   Write MRT template to file
  --w_size         Window size in pixels (1-sided). Default=2000
```

For example, the following CLI input creates output 'combinations01' based on 2 input rasters: 

`python COMBINE.py c:/apps/test_combine/combinations01.tif c:/apps/test_data/top10.tif c:/apps/test_data/beheertypen.tif`

The output would be:

```
c:\apps\test_combine\combinations01.tif <<-- the Geospatial raster
c:\apps\test_combine\combinations01.csv <<-- CSV file with key to the output TIFF
```

For testing purposes, add ```--test``` to the Command Line command. This restricts the tool to only 20 sub-windows.

Users within the WUR network can use the following commands for testing purposes.

Testing run: ```python COMBINE.py <output_location>.tif w:\PROJECTS\DUIN\Data\NATUUR\BasisbestandNatuurLandschap\BNL2020\Bedrijventerrein_Pos5_20210315time162111.tif w:\PROJECTS\DUIN\Data\NATUUR\BasisbestandNatuurLandschap\BNL2020\OAD_Bebkom_Top10_Vlak_2020_sept_Pos4_20210302time141928.tif w:\PROJECTS\DUIN\Data\NATUUR\BasisbestandNatuurLandschap\BNL2020\Top10_Vlak_2020_sept_Pos1_20210325time161731.tif --test```

Full run: ```python COMBINE.py <output_location>.tif w:\PROJECTS\DUIN\Data\NATUUR\BasisbestandNatuurLandschap\BNL2020\Bedrijventerrein_Pos5_20210315time162111.tif w:\PROJECTS\DUIN\Data\NATUUR\BasisbestandNatuurLandschap\BNL2020\OAD_Bebkom_Top10_Vlak_2020_sept_Pos4_20210302time141928.tif w:\PROJECTS\DUIN\Data\NATUUR\BasisbestandNatuurLandschap\BNL2020\Top10_Vlak_2020_sept_Pos1_20210325time161731.tif```

## Details
The Combine tool verifies that all input rasters have an identical extent and resolution, and quits if this is not the case. 

The Combine tool subdivides the geographical extent of the input rasters into many `windows`, which are processed in parallel. The default window size is 2.000 X 2.000 pixels. The tool assumes that the input raster width and height are conmensurable with the window size. This is *assumed* but not *verified*.

The Combine tool writes temporary files to disk. Around ~60GB local storage capacity is required.

Run time for combining 7 rasters with shape 112.000 X 130.000 on a 16GB RAM laptop is **5.5 hours**. 

## Development
Possible future developments include: 

 * ~~user-defined window sizes~~ done, sept 2023.
 * `DBF` format attribute table as output 
 * verify input raster-size is multitude of window-size.

 ## How the Combine tool works
This section explains how the Combine tool works. Let's look at the two intput categorical rasters again: 

![combined_raster](resources/input_rasters.JPG)

Firstly, the tool designs Windows of equal size that subdivide the input rasters. The default Window size in the tool is 2.000 x 2.000 (rows x columns). In this example we take two windows of size 4 rows * 2 columns. The position of the Window within the image is recorded as the top-left index. For Window01: `row 0, column 0` and for Window02: `row 0, column 2`. 

![Windows](resources/Windows.JPG)

The following occurs for each window, and is processed in parallel. 

A chunk of each input raster, as specified by the Window, is read into memory. These sub-arrays are 'stacked' along the third-dimension, so that the combinations of values in the input-rasters can be determined. 

For Window01 the combinations are:

`combinations = (11, 142), (11, 142), (13, 160), (11, 143), (13, 144), (10, 100), (13, 144), (10, 143)`

Removing duplicates, the following unique combinations remain:

`unique_combinations = (11, 142), (13, 160), (11, 143), (13, 144), (10, 100), (10, 143)`

A dictionary is built where the unique combinations are the _keys_ and their enumeration the values.

|key|value|
|-----|-----|
(11, 142)|1
(13, 160)|2
(11, 143)|3
(13, 144)|4
(10, 100)|5
(10, 143)|6

Applying this dictionary to the _stack_ of sub-arrays, yields a `4x2`array: 

![W01Reclassed.JPG](resources/W01_reclassed.JPG)

Applying the same procedure to the second window yields the following dictionary and array: 

|key|value|
|-----|-----|
(10, 160)|1
(11, 144)|2
(12, 161)|3
(10, 100)|4
(11, 100)|5
(11, 142)|6

![W02Reclassed](resources/W02_reclassed.JPG)

The resulting arrays are written to file as `*.npz` files. The dictionaries linking combinations to enurations are stored in memory as property of the Window. 

Once all Windows are processed, all combinations occuring in the input rasters are known (as the keys of the dictionaries). In the example, there are 10 unique combinations. A new dictionary is construed to map the unique combinations to an output value: 

|key|value|
|-----|-----|
(11, 142)|1
(13, 160)|2
(11, 143)|3
(13, 144)|4
(10, 100)|5
(10, 143)|6
(10, 160)|7
(11, 144)|8
(12, 161)|9
(11, 100)|10

The dictionary of each Window is updated to map the Window-specific output value to the Joint-output value. For Window01:

|Output Value Window01|Joint-Output value|
|-----|-----|
1|1
2|2
3|3
4|4
5|5
6|6

and for Window02:

|Output Value Window02|Joint-Output value|
|-----|-----|
1|7
2|8
3|9
4|5
5|10
6|1

The temporary array of each Window is re-read from file and remapped according to the updated dictionary. 

For Window01:

![Window01 to joint](resources/W01_reclassed.JPG)

and Window02:

![Window02 to joint](resources/W02_reclassed_to_joint.JPG)


Writing the output to file occurs in a single Task that loops over all Windows. This is because writing to a GeoTiff from multiple processes is not possible. 

The remapped array is positioned within the overall image according to the Window specs and then written to file. This creates the output array:

![Output](resources/JointOutputArray.JPG)


## Support
Get in touch with [Hans](https://www.wur.nl/en/Persons/Hans-dr.-HD-Hans-Roelofsen.htm). 

## Authors and acknowledgment
Thanks to Johan B: https://stackoverflow.com/a/59962140/981768

## License
CC BY-NC-SA 4.0

