import datetime
import os

import prefect
from prefect import Flow, unmapped, Parameter
from prefect.executors import LocalDaskExecutor

try:
    from src.input_pathway import get_source_rasters, verify_target
    from src.combinations import (
        read_write_combination_chunk,
        build_combination_dict,
        dispose_temporary_arrays,
        build_value_count_dataframe,
    )
    from src.windows import (
        get_rw_windows,
        update_window_to_joint_dict,
        update_window_joint_value_count,
    )
    from src.attribute_table import make_attribute_table, get_max_value
    from src.target_raster import map_chunks_to_joint
    from src.mrt_template import mrt_template
except ModuleNotFoundError:
    from input_pathway import get_source_rasters, verify_target
    from combinations import (
        read_write_combination_chunk,
        build_combination_dict,
        dispose_temporary_arrays,
        build_value_count_dataframe,
    )
    from windows import (
        get_rw_windows,
        update_window_to_joint_dict,
        update_window_joint_value_count,
    )
    from attribute_table import make_attribute_table, get_max_value
    from target_raster import map_chunks_to_joint
    from mrt_template import mrt_template

with Flow("COMBINE") as COMBINE:
    destination = Parameter("destination")
    source_rasters = Parameter("sources")
    testing = Parameter("testing")
    template = Parameter("mrt_template")
    window_size = Parameter("window_size")

    # Verify target destination
    verified_target = verify_target(destination)
    verified_target.set_dependencies(upstream_tasks=[destination])

    # Get the source rasters as a list
    source_rasters = get_source_rasters(source_rasters)

    with prefect.case(template, True):
        write_mrt_template = mrt_template(
            source_rasters,
            destination,
        )
        write_mrt_template.set_dependencies(upstream_tasks=[verified_target])

    # Build list of read/write windows
    rw_windows = get_rw_windows(
        source_rasters=source_rasters,
        sample=testing,
        window_width=window_size,
        window_height=window_size,
    )

    # For each window, read the source rasters, make a combined-raster and write combined-raster to temporary file
    solo_combis = read_write_combination_chunk.map(
        rw_windows, unmapped(source_rasters), unmapped(destination)
    )
    solo_combis.set_dependencies(
        upstream_tasks=[rw_windows, source_rasters, verified_target]
    )

    # Build a joint combination dict in which the combination tuples for all windows are the keys
    combination_dict = build_combination_dict(read_windows=rw_windows)
    combination_dict.set_dependencies(upstream_tasks=[solo_combis])

    # Update each window with the joint combinatino dictionary
    updated_windows_pt1 = update_window_to_joint_dict.map(
        rw_windows, unmapped(combination_dict)
    )
    updated_windows_pt1.set_dependencies(upstream_tasks=[combination_dict])

    # Update the value count with the joint combinatino dict for each window
    updated_windows_pt2 = update_window_joint_value_count.map(rw_windows)
    updated_windows_pt2.set_dependencies(
        upstream_tasks=[updated_windows_pt1, combination_dict]
    )

    # Build DataFrame with the Count of each key in the joint combination dict over all windows.
    value_count_df = build_value_count_dataframe(read_windows=rw_windows)
    value_count_df.set_dependencies(upstream_tasks=[updated_windows_pt2])

    # Build the attribute table and write to CSV
    attribute_table = make_attribute_table(
        combination_dict, value_count_df, source_rasters, destination
    )
    attribute_table.set_dependencies(upstream_tasks=[combination_dict, value_count_df])

    # Determine the maximum value in the combination raster
    max_value = get_max_value(combination_dict)
    max_value.set_dependencies(upstream_tasks=[combination_dict])

    # Map each chunk to the output file
    chunks_to_joint = map_chunks_to_joint(
        rw_windows,
        source_rasters,
        destination,
        max_value,
    )
    chunks_to_joint.set_dependencies(upstream_tasks=[max_value, updated_windows_pt1])

    garbage_disposal = dispose_temporary_arrays.map(rw_windows, unmapped(destination))
    garbage_disposal.set_dependencies(upstream_tasks=[chunks_to_joint])


if __name__ == "__main__":
    """Run the COMBINE DAG.
    The COMBINE dag is implemented using Prefect (https://docs.prefect.io/)

    Parameters
    ----------
    destination: str
        Path to destination *.tif file
    source_raster01: Path to first source raster
    source_raster02: Path to second source raster
    ...: path to subsequent source  rasters

    Returns
    -------
    """

    import argparse

    parser = argparse.ArgumentParser(
        prog="Open Source COMBINE alternative. By: Hans Roelofsen"
    )
    parser.add_argument("destination", type=str, help="path to destination *.tif file")
    parser.add_argument(
        "sources", type=str, help="path to destination source files", nargs="+"
    )
    parser.add_argument("--test", action="store_true", help="test run with 20 windows")
    parser.add_argument(
        "--mrt_template", action="store_true", help="Write MRT template to file."
    )
    parser.add_argument(
        "--w_size",
        help="Window size in nr. pixels. For one side only. Windows are square.",
        type=int,
        default=2000,
    )
    args = parser.parse_args()

    print("\n==WENR COMBINE TOOL==\n")

    t0 = datetime.datetime.now()
    print(f'starting @ {t0.strftime("%d-%B-%Y %H:%M:%S")}h')

    COMBINE.executor = LocalDaskExecutor()
    COMBINE.run(
        parameters=dict(
            destination=args.destination,
            sources=args.sources,
            testing=args.test,
            mrt_template=args.mrt_template,
            window_size=args.w_size,
        )
    )

    t1 = datetime.datetime.now()
    d = t1 - t0
    print(
        f'done @ {t1.strftime("%d-%B-%Y %H:%M:%S")}, after {str(d).split(":")[0]} hours, {str(d).split(":")[1]} minutes and {str(d).split(":")[2]} seconds.'
    )
