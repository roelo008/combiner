import os
import prefect
import rasterio as rio
from prefect import task
from rasterio import shutil as rioshutil
from abc import ABC, abstractmethod
from prefect.engine import signals
import pandas as pd
import numpy as np


class SourceRasters:
    def __init__(self, source_list: list):
        self.source_list = [s for s in source_list]
        self.procedures = [
            VerifyAsGeospatialRaster(source_list=self.source_list),
            VerifyMatchingBounds(source_list=self.source_list),
            VerifyMatchingResolution(source_list=self.source_list),
        ]
        self.geospatial_profile = getattr(rio.open(self.source_list[0]), "profile")
        self.dtypes = dict(
            zip(
                self.source_list,
                [
                    np.dtype(getattr(rio.open(x), "profile")["dtype"])
                    for x in self.source_list
                ],
            )
        )

    def verify_input(self):
        for procedure in self.procedures:
            procedure.execute()

        # Gather status of each procedure
        procedure_results = []
        for procedure in self.procedures:
            procedure_results.append(getattr(procedure, "status"))

        # If any procedure has failed, raise FAIL signal to end flow
        if not all(procedure_results):
            self.passed = False
            raise signals.FAIL(
                "Cannot proceed due to errors in user input. See Errors.txt log file."
            )


class VerificationProcedure(ABC):
    @abstractmethod
    def execute(self):
        pass


class VerifyAsGeospatialRaster(VerificationProcedure):
    def __init__(self, source_list):
        self.rasters = source_list
        self.status = True

    def execute(self):
        logger = prefect.context.get("logger")

        for raster in self.rasters:
            check, message = is_geospatial_raster(raster, msg=True)
            if check:
                logger.info(message)
            else:
                logger.error(message)
                self.status = False


class VerifyMatchingBounds(VerificationProcedure):
    def __init__(self, source_list):
        self.rasters = source_list
        self.status = True

    def execute(self):
        logger = prefect.context.get("logger")
        matching_bounds, msg = rasters_have_identical_bounds(self.rasters)
        if matching_bounds:
            logger.info(msg)
        else:
            for submessage in msg.strip(";").split(";"):
                logger.error(submessage)
                self.status = False


class VerifyMatchingResolution(VerificationProcedure):
    def __init__(self, source_list):
        self.rasters = source_list
        self.status = True

    def execute(self):
        logger = prefect.context.get("logger")

        reference_resolution = getattr(
            getattr(rio.open(self.rasters[0]), "transform"), "a"
        )
        resolutions = []
        messages = []
        for raster in self.rasters:
            has_resolution, msg = raster_has_resolution(raster, reference_resolution)
            resolutions.append(has_resolution)
            messages.append(msg)

        if not all(resolutions):
            logger.error("Rasters have unequal resolutions")
            self.status = False


def is_geospatial_raster(file_path: str, msg=False) -> (bool, str):
    """
    Is a file a gdal compatible geospatial raster?
    Parameters
    ----------
    file_pathL target file path

    Returns
    -------

    """
    is_geospatial_raster = rioshutil.exists(file_path)
    if is_geospatial_raster and msg:
        return True, f"{file_path} is a geospatial raster"
    elif is_geospatial_raster and not msg:
        return True
    elif not is_geospatial_raster and msg:
        return False, f"{file_path} is not a geospatial raster"
    else:
        return False


def rasters_have_identical_bounds(rasters: list) -> (bool, str):
    """
    Verify that all rasters have identical bound
    Parameters
    ----------
    rasters: list with one or more paths to raster files on disk

    Returns
    -------

    """

    rasters_objects = [
        rio.open(raster_file)
        for raster_file in rasters
        if rioshutil.exists(raster_file)
    ]

    raster_names = [
        os.path.basename(getattr(raster_obj, "name")) for raster_obj in rasters_objects
    ]
    msg = ""
    out = True

    bound_sides = ["left", "bottom", "right", "top"]

    bounds = pd.DataFrame.from_dict(
        data={
            bound: dict(
                zip(
                    raster_names,
                    [getattr(getattr(raster_obj, "bounds"), bound) for raster_obj in rasters_objects],
                )
            )
            for bound in bound_sides
        }
    )

    for bound in bound_sides:
        if len(set(bounds[bound])) != 1:
            out = False
            msg += "Unqueal {0} bounds: {1} for {2};".format(
                bound,
                ", ".join([str(i) for i in bounds[bound].to_list()]),
                ", ".join([str(i) for i in bounds.index.to_list()]),
            )
    if out:
        msg = "Raster bounds identical"

    return out, msg


def raster_has_resolution(file_path: str, target_resolution) -> (bool, str):
    """
    Check if a raster has target resolution
    Parameters
    ----------
    file_path: target file path
    target_resolution: expected resolution of the raster

    Returns
    -------

    """

    if is_geospatial_raster(file_path):
        raster_resolution = getattr(rio.open(file_path), "res")[0]
        has_target_resolution = raster_resolution == target_resolution
        if has_target_resolution:
            out = True
            msg = f"{os.path.basename(file_path)} has target resolution."
        else:
            out = False
            msg = f"{os.path.basename(file_path)} resolution {raster_resolution}m does not meet target resolution {target_resolution}."
        return out, msg
    else:
        return False, f"{os.path.basename} is not a geospatial raster"


@task
def get_source_rasters(source_list: list) -> SourceRasters:
    source_rasters = SourceRasters(source_list=source_list)
    source_rasters.verify_input()
    return source_rasters


@task
def verify_target(target: str):
    """
    Verify that target raster points to a valid directory, but the file itself doesnt exist
    """

    logger = prefect.context.get("logger")

    valid_directory = os.path.isdir(os.path.dirname(target))
    existing_file = os.path.isfile(target)

    if valid_directory and not existing_file:
        logger.info("Valid target file")

    elif not valid_directory:
        logger.error(
            f"Invalid target directory: {os.path.isdir(os.path.dirname(target))}"
        )

    elif existing_file:
        logger.error(f"Target file already exists")

    if (not valid_directory) or (existing_file):
        raise signals.FAIL("Cannot proceed due to errors. See log file for details.")
