import os
import pickle
import numpy as np
import pandas as pd
import rasterio as rio
from rasterio.windows import Window
import prefect
from prefect import task, Parameter

try:
    from windows import CombineWindow
    from input_pathway import SourceRasters
except ModuleNotFoundError:
    from src.windows import CombineWindow
    from src.input_pathway import SourceRasters


@task
def read_write_combination_chunk(
    read_window: CombineWindow,
    source_rasters: SourceRasters,
    destination: Parameter,
):
    """
    Write a combined arrays + dictionary to file
    """

    logger = prefect.context.get("logger")

    destination_file = f"{os.path.splitext(destination)[0]}_{read_window.nr:05}.npy"

    # Read window from all source-rasters and store in list of arrays
    # Then zip to get vertical stacks
    zipped_arrays = list(
        zip(
            *[
                rio.open(source_raster).read(1, window=read_window.rio_window).flatten()
                for source_raster in source_rasters.source_list
            ]
        )
    )

    # Dictionary between unique-combinations and an enumerated output value
    d = {combi: n for n, combi in enumerate(set(zipped_arrays), start=1)}

    # Array with combi-tuples remapped to combination-value
    combi_array = np.array([d[array] for array in zipped_arrays])

    # Create combined array and save to temporary file
    with open(destination_file, "wb") as dest:
        np.save(dest, combi_array)

    # Save this specific dictionary as a property of the window
    read_window.save_window_combi_dict(combi_dict=d)

    # Save value counts of the combined array as property of the window
    read_window.save_solo_value_counts(np.unique(combi_array, return_counts=True))


@task
def dispose_temporary_arrays(rw_window: CombineWindow, destination: Parameter):
    """ """
    os.remove(f"{os.path.splitext(destination)[0]}_{rw_window.nr:05}.npy")


@task
def build_combination_dict(read_windows: list) -> dict:
    """
    Gather solo-combination dictionaries from all Windows and return joint dictionary
    window01.solo_combi_dict = {(0,0,0): 1,
                                (0,1,1): 2}
    window02.solo_combi_dict = {(10,101,1500): 1,
                                (10,200,1400): 2
                                (0,1,1): 3}
    combination_dict = {(0,0,0): 1,
                        (0,1,1): 2,
                        (10,101,1500): 3,
                        (10,200,1400): 4}
    """

    u_combis = set()
    for d in [window.solo_combi_dict for window in read_windows]:
        u_combis.update(list(d.keys()))
    return {combi: n for n, combi in enumerate(u_combis, start=1)}


@task
def build_value_count_dataframe(read_windows: list) -> pd.DataFrame:
    """
    Count how often each joint-combi value occurs in all the windows
    """

    value_count = pd.DataFrame({}, columns=["Count"])
    for window in read_windows:
        value_count = value_count.combine(
            pd.DataFrame.from_dict(
                window.joint_value_counts, orient="index", columns=["Count"]
            ),
            func=lambda x, y: x + y,
            fill_value=0,
        )
    return value_count
