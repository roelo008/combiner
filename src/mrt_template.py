import os
from prefect import task
import pandas as pd

from input_pathway import SourceRasters

TEMPLATE_DEFAULTS = {
    "newvalue": [1, 2, 3],
    "description": ["<class description goes here>"] * 3,
    "remark": ["<optional remark>"] * 3,
    "reclass": ["<1/0 to enable/disable this reclass rule>"] * 3,
    "volgnummer": [1, 2, 3],
}


@task
def mrt_template(
    source_rasters: SourceRasters, destination: str, upstream_tab_name: str = "upstream"
) -> None:
    """
    Write Multi-Reclass Tool (MRT) reclass Excel file template to disk, using the names of the source-rasters as
    columns.
    Parameters
    ----------
    source_rasters: SourceRasters object
    destination: path and filename for the combined Raster
    upstream_tab_name: name of the tab in the Excel file/

    Returns
    -------

    """

    with pd.ExcelWriter(
        f"{os.path.splitext(destination)[0]}_mrt_template.xlsx", mode="w"
    ) as writer:
        source_raster_names = [
            os.path.splitext(os.path.basename(s))[0] for s in source_rasters.source_list
        ]
        source_raster_defaults = [["n", "n", "n"]] * len(source_raster_names)

        pd.concat(
            [
                pd.DataFrame.from_dict(TEMPLATE_DEFAULTS, orient="index"),
                pd.DataFrame.from_dict(
                    dict(zip(source_raster_names, source_raster_defaults)),
                    orient="index",
                ),
            ]
        ).T.to_excel(writer, sheet_name=upstream_tab_name, index=False)
