import os
from prefect import task
import rasterio as rio
import prefect
import numpy as np
import datetime

try:
    from input_pathway import SourceRasters
    from windows import CombineWindow
except ModuleNotFoundError:
    from src.input_pathway import SourceRasters
    from src.windows import CombineWindow


@task
def map_chunks_to_joint(
    rw_windows: list,
    source_rasters: SourceRasters,
    destination: str,
    max_value: int,
):
    """
    Load a npy array from file, apply the joint combination dict to it and write as chunk to the full output raster
    """

    logger = prefect.context.get("logger")

    with rio.open(
        destination,
        "w",
        driver="GTiff",
        width=source_rasters.geospatial_profile["width"],
        height=source_rasters.geospatial_profile["height"],
        count=1,
        dtype=rio.dtypes.get_minimum_dtype([0, max_value]),
        transform=source_rasters.geospatial_profile["transform"],
        compress="LZW",
        crs=source_rasters.geospatial_profile["crs"],
        BIGTIFF="YES",
    ) as dest:
        dest.update_tags(
            creation_date=datetime.datetime.now().strftime("%d-%b-%Y_%H:%M:%S"),
            creator=os.environ.get("USERNAME"),
            created_with="WENR Combine tool",
            **dict(
                zip(
                    [
                        f"sourceraster{i:2}"
                        for i, _ in enumerate(source_rasters.source_list, start=1)
                    ],
                    source_rasters.source_list,
                )
            ),
        )
        for rw_window in rw_windows:
            logger.info(
                f"Writing chunk {rw_window.nr} out of {rw_window.total} to file."
            )

            with open(
                f"{os.path.splitext(destination)[0]}_{rw_window.nr:05}.npy", "rb"
            ) as f:
                combi_array = np.load(f)

            target_values = np.vectorize(rw_window.joint_combi_dict.__getitem__)(
                combi_array
            )

            dest.write(
                np.array(target_values).reshape(
                    (rw_window.rio_window.height, rw_window.rio_window.width)
                ),
                indexes=1,
                window=rw_window.rio_window,
            )
