import pandas as pd
from prefect import task
import os
import numpy as np

try:
    from input_pathway import SourceRasters
except ModuleNotFoundError:
    from src.input_pathway import SourceRasters


@task
def make_attribute_table(
    combi_dict: dict,
    value_counts: pd.DataFrame,
    source_rasters: SourceRasters,
    destination: str,
):
    """

    :param combi_dict:
    :param value_counts:
    :param source_rasters:
    :param destination:
    :return:
    """

    column_names = [
        os.path.splitext(os.path.basename(src_raster))[0]
        for src_raster in source_rasters.source_list
    ]
    tab = pd.DataFrame.from_records(
        data=[x for x in combi_dict.keys()],
        columns=column_names,
        index=[v for v in combi_dict.values()],
    ).astype(dict(zip(column_names, [v for _, v in source_rasters.dtypes.items()])))

    tab = tab.join(value_counts).astype({"Count": np.uint64})
    #    tab.to_clipboard(excel=True)
    tab.to_csv(
        f"{os.path.splitext(destination)[0]}.csv",
        index=True,
        index_label="Value",
        sep=",",
        header=True,
    )


@task
def get_max_value(combi_dict: dict) -> int:
    """

    :param combi_dict:
    :return:
    """
    return len(combi_dict)
