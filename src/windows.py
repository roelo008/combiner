import random
import prefect
import affine
import numpy as np
import rasterio as rio
from prefect import task
from rasterio.windows import Window

try:
    from input_pathway import SourceRasters
except ModuleNotFoundError:
    from src.input_pathway import SourceRasters


class CombineWindow:
    """
    A single read/write window and its associated unique combinations
    """

    def __init__(
        self,
        row_start: int,
        row_stop: int,
        column_start: int,
        column_stop: int,
        nr: int,
        totals: int,
        top_left_x: int,
        top_left_y: int,
        resolution: int,
    ):
        self.rio_window = Window.from_slices(
            (row_start, row_stop), (column_start, column_stop)
        )
        self.unique_combinations = None
        self.solo_combi_dict = None
        self.joint_combi_dict = None
        self.solo_value_counts = None
        self.joint_value_counts = None
        self.nr = nr
        self.total = totals

        self.transform = affine.Affine(
            a=resolution, b=0, c=top_left_x, d=0, e=resolution * -1, f=top_left_y
        )

    def save_window_combi_dict(self, combi_dict: dict):
        self.solo_combi_dict = combi_dict

    def save_solo_value_counts(self, value_counts: tuple):
        """
        self.solo_value_counts = {1: 10,
                                  2: 12,
                                  3: 50}
        Dictionary between the *keys* in self.solo_combi_dict and their *count* in the solo-combined array of the window
        (which is written to file as *.npz).
        """
        self.solo_value_counts = dict(zip(value_counts[0], value_counts[1]))

    def map_to_joint_combination_dict(self, joint_combination_dict: dict):
        """
        self.solo_combi_dict = {(1,1,2): 1,
                                (1,2,1): 2,
                                (0,0,1): 3}
        joint_combination_dict = {(1,1,1): 1,
                                  (1,1,2): 2,
                                  (1,2,2): 3,
                                  (1,2,1): 4,
                                  (2,2,2): 5,
                                  (0,0,0): 6,
                                  (0,0,1): 7}
        self.joint_combi_dict = {1: 2,
                                 2: 4,
                                 3: 7}
        """
        self.joint_combi_dict = {
            v: joint_combination_dict[k] for k, v in self.solo_combi_dict.items()
        }

    def save_joint_value_counts(self):
        """
        Return value counts of the combi-array of this window, with joint-combination values as keys.

        self.solo_combi_dict = {(1,1,2): 1,
                                (1,2,1): 2,
                                (0,0,1): 3}
        self.solo_value_counts = {1: 10,
                                  2: 12,
                                  3: 50}
        self.joint_combi_dict = {1: 2,
                                 2: 4,
                                 3: 7}
        Return:
              {2: 10,
               4: 12,
               7: 50}

        """

        self.joint_value_counts = {
            v: self.solo_value_counts[k] for k, v in self.joint_combi_dict.items()
        }


@task
def get_rw_windows(
    source_rasters: SourceRasters,
    window_height: int = 2000,
    window_width: int = 2000,
    sample: bool = False,
) -> list:
    """ """

    logger = prefect.context.get("logger")

    row_starts = range(0, source_rasters.geospatial_profile["height"], window_height)
    column_starts = range(0, source_rasters.geospatial_profile["width"], window_width)

    #    row_starts = range(0, height, window_height)
    #    column_starts = range(0, width, window_width)

    windows = []
    n = 0
    total_windows = np.multiply(len(row_starts), len(column_starts))
    for row_start in {True: random.sample(row_starts, 5), False: row_starts}[sample]:
        for column_start in {
            True: random.sample(column_starts, 4),
            False: column_starts,
        }[sample]:
            windows.append(
                CombineWindow(
                    row_start=row_start,
                    row_stop=row_start + window_height,
                    column_start=column_start,
                    column_stop=column_start + window_width,
                    nr=n,
                    totals=total_windows,
                    top_left_x=(
                        getattr(rio.open(source_rasters.source_list[0]), "transform")
                        * (column_start, row_start)
                    )[0],
                    top_left_y=(
                        getattr(rio.open(source_rasters.source_list[0]), "transform")
                        * (column_start, row_start)
                    )[1],
                    resolution=getattr(
                        getattr(rio.open(source_rasters.source_list[0]), "transform"),
                        "a",
                    ),
                )
            )
            n += 1
    logger.info(f"Created {n} windows of size {window_height}X{window_width}")

    return windows


@task
def update_window_to_joint_dict(rw_window: CombineWindow, joint_dict: dict):
    """ """

    rw_window.map_to_joint_combination_dict(joint_combination_dict=joint_dict)


@task
def update_window_joint_value_count(rw_window: CombineWindow):
    """ """
    rw_window.save_joint_value_counts()
